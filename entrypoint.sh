#!/bin/bash

## Function definition ##

URL="https://cloudnetservice.eu/cloudnet/version/release/${CLOUDNET_TAG}/CloudNet.zip"

function setup_cloudnet_wrapper() {
	if [ ! -f $SERVER_JARFILE ]; then
        if [[ -e "dist.zip" ]]; then
            rm dist.zip
        fi
        
		echo "Downloading Wrapper"
		echo "Downloading: $URL"
		curl -o dist.zip -sSL $URL
        
        echo "Extracting wrapper..."
        unzip dist.zip 'CloudNet-Wrapper/*' -d ./
	fi
}

function setup_cloudnet_master() {
	if [ ! -f $SERVER_JARFILE ]; then
		if [[ -e "dist.zip" ]]; then
            rm dist.zip
        fi
        
        echo "Downloading Master"
		echo "Downloading: $URL"
		curl -o dist.zip -sSL $URL
        
        echo "Extracting master..."
        unzip dist.zip 'CloudNet-Master/*' -d ./
	fi
}

## RUNTIME ##
cd /home/container

if [ "${whoami}" == "container" ]; then
	git config --global user.email "technik@fearnixx.de"
	git config --global user.name "FearNixx Technik"
fi

# Evaluate update/download/install
if [ ! -z $SERVER_BRANCH ]; then
	echo "Detected SERVER_BRANCH: ${SERVER_BRANCH}"
	BASEPATH="/home/container/"
	case "$SERVER_BRANCH" in
		MASTER)
			export SERVER_JARFILE="${BASEPATH}/CloudNet-Master/CloudNet-Master.jar"
			setup_cloudnet_master
		;;
		WRAPPER)
			export SERVER_JARFILE="${BASEPATH}/CloudNet-Wrapper/CloudNet-Wrapper.jar"
			setup_cloudnet_wrapper
		;;
		*)
			echo "Unknown server branch: ${SERVER_BRANCH}"
			exit 1
	esac
else
	echo "Error: SERVER_BRANCH not set!"
	exit 1
fi

# Output Current Java Version
java -version

# Make internal Docker IP address available to processes.
export INTERNAL_IP=`ip route get 1 | awk '{print $NF;exit}'`
echo "IP today: $INTERNAL_IP"

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
eval ${MODIFIED_STARTUP}

# Example startup line: java -Xmx{{SERVER_XMX}}M -Xms{{SERVER_XMS}}M {{EXTRA_JVM}} -jar {{SERVER_JARFILE}} {{EXTRA_APP}}
